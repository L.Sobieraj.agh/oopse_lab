#include <iostream>
#include <string>
#include <fstream>

using namespace std;

void showPrompt();
void getUserInput();

class Character
{

private:
    string name;
    int strength;
    int dexterity;
    int endurance;
    int intelligence;
    int charisma;

public:
    Character(string, int, int, int, int, int);
    string getCharacterDetails();
    string displayCharacter();
};


int main()
{
    showPrompt();
    getUserInput();
    return 0;
    
}

string Character::getCharacterDetails()
{
    return "Name: " + (*this).name + "\nSTRENGTH: " + to_string((*this).strength) + "\nDEXTERITY: " + to_string((*this).dexterity) + "\nENDURANCE: " + to_string((*this).endurance) + "\nINTELLIGENCE: " + to_string((*this).intelligence) + "\nCHARISMA: " + to_string((*this).charisma);
}

string Character::displayCharacter()
{
    return getCharacterDetails();
}

Character::Character(string name, int str, int dex, int end, int intel, int charm)
{
    (*this).name = name;
    (*this).strength = str;
    (*this).dexterity = dex;
    (*this).endurance = end;
    (*this).intelligence = intel;
    (*this).charisma = charm;
}

void showPrompt()
{
    cout << "Choose option:\n\n1. New character\n2. Load Character\n\n";
}

void getUserInput()
{
    int userChoice;
    do
    {
        cout << "Choice: ";
        cin >> userChoice;
    } while (userChoice != 1 && userChoice != 2);

    if (userChoice == 1)
    {
        string name;
        int str, dex, end, intel, charm;

        cout << "\nEnter your hero's stats:\n"
             << "\n";
        cout << "Name: ";
        cin >> name;
        cout << "STRENGTH: ";
        cin >> str;
        cout << "DEXTERITY: ";
        cin >> dex;
        cout << "ENDURANCE: ";
        cin >> end;
        cout << "INTELLIGENCE: ";
        cin >> intel;
        cout << "CHARISMA: ";
        cin >> charm;

        Character test(name, str, dex, end, intel, charm);
        string result = name + ".txt";
        ofstream file;

        file.open(result, ios::out | ios::trunc);
        if (!file.good())
        {
            cout << "File I/O Error: Couldn't save character file.\n";
        }
        else
        {
            file << name << "\n"
                << str << "\n"
                << dex << "\n"
                << end << "\n"
                << intel << "\n"
                << charm;
            file.close();
            cout  << "\nCharacter file saved.\n";
        }
    }

    if (userChoice == 2)
    {
        cout  << "\nLoad Character\n"
             << "\n";
        string name;
        cout << "Name: ";
        cin >> name;

        fstream file;
        file.open(name + ".txt", ios::in);
        if (!file.good())
        {
            cout << "Couldn't load character file.\n";
        }
        else
        {
            int str, dex, end, inte, cha;
            file >> name >> str >> dex >> end >> inte >> cha;
            Character c(name, str, dex, end, inte, cha);

            cout << "Character loaded:\n\n"
                << c.getCharacterDetails() << "\n";
        }
    }
}

