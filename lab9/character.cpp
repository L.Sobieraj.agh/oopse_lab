#include <iostream>
#include <time.h>
#include <string>
#include <fstream>

using namespace std;

void showPrompt();

class Mage;
class Warrior;
class Berserker;
class Thief;

class Hero
{
private:
    string name;
    int strength;
    int dexterity;
    int endurance;
    int intelligence;
    int charisma;
    

public:
    Hero();
    Hero(string, int, int, int, int, int);
    void updateAttributes(string, int, int, int, int, int);
    string getCharacterDetails();
    void generateFile();

    friend class Mage;
    friend class Warrior;
    friend class Berserker;
    friend class Thief;
};

Hero::Hero()
{
    name = "noname";
    strength = 0;
    dexterity = 0;
    endurance = 0;
    intelligence = 0;
    charisma = 0;
}

Hero::Hero(string name, int str, int dex, int end, int intel, int csma)
{
    (*this).name = name;
    (*this).strength = str;
    (*this).dexterity = dex;
    (*this).endurance = end;
    (*this).intelligence = intel;
    (*this).charisma = csma;
}

void Hero::updateAttributes(string n, int s, int d, int e, int i, int c)
{
    (*this).name = n;
    (*this).strength = s;
    (*this).dexterity = d;
    (*this).endurance = e;
    (*this).intelligence = i;
    (*this).charisma = c;
}

string Hero::getCharacterDetails()
{
    return name + " =>\tSTR: " + to_string(strength) + "\tDEX: " + to_string(dexterity) + "\tEND: " + to_string(endurance) + "\tINT: " + to_string(intelligence) + "\tCHA: " + to_string(charisma);
}

class Mage
{
public:
    void upgrade(Hero&);
};

void Mage::upgrade(Hero& h)
{
    h.intelligence += 1;
}

class Warrior
{
public:
    void upgrade(Hero&);
};

void Warrior::upgrade(Hero& h)
{
    h.endurance += 1;
}

class Berserker
{
public:
    void upgrade(Hero&);
};

void Berserker::upgrade(Hero& h)
{
    h.strength += 1;
}

class Thief
{
public:
    void upgrade(Hero&);
};

void Thief::upgrade(Hero& h)
{
    h.dexterity += 1;
}

void Hero::generateFile()
{
    string filename = name + ".txt";
    ofstream file;
    file.open(filename, ios::out | ios::trunc);
    file << name << "\n"
        << strength << "\n"
        << dexterity << "\n"
        << endurance << "\n"
        << intelligence << "\n"
        << charisma;
    file.close();
    cout  << "\nCharacter file generated.\n";
}

class Monster : public Hero
{
public:
    Monster();
};

Monster::Monster()
{
    
    int intelligence = rand() % 10 + 1;
    int charisma = rand() % 10 + 1;
    int strength = rand() % 10 + 1;
    int dexterity = rand() % 10 + 1;
    int endurance = rand() % 10 + 1;
    updateAttributes("Monster", strength, dexterity, endurance, intelligence, charisma);
}

void showPrompt()
{
    cout << "C++ RPG:\n\n\t1. Generate new character\n\t2. Load Previously saved character\n\t3. Create monsters\n\t4. Quit\n\n";
}

int main()
{
    srand(time(NULL));
    showPrompt();

    int userChoice;
    do
    {
        cout << "Please enter one of these numbers (1, 2, 3, 4): ";
        cin >> userChoice;
        if (userChoice == 4)
        {
            return 0;
        }
    } while (userChoice != 1 && userChoice != 2 && userChoice != 3);

    Hero heroRef;

    if (userChoice == 1)
    {
        string name;
        int strength, dexterity, endurance, intelligence, charisma;

        cout  << "\nEnter details for new character\n"
             << "\n";
        cout << "Name: ";
        cin >> name;
        cout << "STRENGTH: ";
        cin >> strength;
        cout << "DEXTERITY: ";
        cin >> dexterity;
        cout << "ENDURANCE: ";
        cin >> endurance;
        cout << "INTELLIGENCE: ";
        cin >> intelligence;
        cout << "CHARISMA: ";
        cin >> charisma;

        heroRef = Hero(name, strength, dexterity, endurance, intelligence, charisma);

        heroRef.generateFile();
    }

    if (userChoice == 2)
    {
        cout  << "\nEnter the name of existing character\n"
             << "\n";
        string name;
        cout << "Name: ";
        cin >> name;

        fstream outFile;
        outFile.open(name + ".txt", ios::in);
        if (!outFile.good())
        {
            cout << "Couldn't load character file.\n";
            return 1;
        }
        else
        {
            int str, dex, end, intel, charm;
            outFile >> name >> str >> dex >> end >> intel >> charm;
            heroRef = Hero(name, str, dex, end, intel, charm);

            cout << "Character load successful:\n\n"
                << heroRef.getCharacterDetails() << "\n";
        }
    }

    if (userChoice == 3)
    {
        char selection = 0;
        do
        {
            Monster monsterData[5];
            cout << "Enemies Created:\n\n";
            for (auto& monst : monsterData)
            {
                cout << monst.getCharacterDetails() << "\n";
            }
            cout << "\nSave to file or retry?\n";
            do
            {
                cout << "Choose (s, r) or (q) to quit: ";
                cin >> selection;
                if (selection == 'q')
                {
                    return 0;
                }
            } while (selection != 's' && selection != 'r');

            if (selection == 's')
            {
                ofstream resultantFile;
                resultantFile.open("Monsters", ios::out | ios::trunc);

                for (auto& monst : monsterData)
                {
                    resultantFile << monst.getCharacterDetails() << "\n";
                }
                cout << "Saved monsters data.\n";
                return 0;
            }

        } while (selection == 'r');
    }

    Mage mageRef;
    Warrior warriorRef;
    Berserker berserkerRef;
    Thief thiefRef;

    char userInp = 0;
    while (userInp != 'q')
    {
        bool alreadyDone = true;
        cout << "\nSelect profession to Level Up Mage (m), Warrior (w), Berserker (b), Thief (t)\n";
        do
        {
            cout << "Choose (m, w, b, t) or (q) to quit: ";
            cin >> userInp;
        } while (userInp != 'm' && userInp != 'w' && userInp != 'b' && userInp != 't' && userInp != 'q');

        if (userInp == 'q')
        {
            continue;
        }

        cout << "\nIncreasing Stats\nNew stats:\n";
        switch (userInp)
        {
        case 'm':
            mageRef.upgrade(heroRef);
            break;
        case 'w':
            warriorRef.upgrade(heroRef);
            break;
        case 'b':
            berserkerRef.upgrade(heroRef);
            break;
        case 't':
            thiefRef.upgrade(heroRef);
            break;
        }
        cout << heroRef.getCharacterDetails() << "\n";
        heroRef.generateFile();
    }

    return 0;
}


