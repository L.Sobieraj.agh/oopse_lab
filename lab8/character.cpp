#include <string>
#include <iostream>
#include <fstream>

using namespace std;

void showPrompt();


class Thief;
class Warrior;
class Mage;
class Berserker;


class Hero
{
private:
    string name;
    int strength;
    int dexterity;
    int endurance;
    int intelligence;
    int charisma;

public:
    Hero();
    Hero(string, int, int, int, int, int);
    string getCharacterDetails();
    string displayCharacter();
    void generateFile();

    friend class Mage;
    friend class Warrior;
    friend class Berserker;
    friend class Thief;
};


void newFeature(Hero);

string Hero::getCharacterDetails()
{
    return "Name: " + name + "\nSTRENGTH: " + to_string(strength) + "\nDEXTERITY: " + to_string(dexterity) + "\nENDURANCE: " + to_string(endurance) + "\nINTELLIGENCE: " + to_string(intelligence) + "\nCHARISMA: " + to_string(charisma);
}

string Hero::displayCharacter()
{
    return getCharacterDetails();
}

Hero::Hero()
{
    name = "noname";
    strength = 0;
    dexterity = 0;
    endurance = 0;
    intelligence = 0;
    charisma = 0;
}

Hero::Hero(string name, int str, int dex, int end, int intel, int charm)
{
    (*this).name = name;
    (*this).strength = str;
    (*this).dexterity = dex;
    (*this).endurance = end;
    (*this).intelligence = intel;
    (*this).charisma = charm;
}


class Mage
{
public:
    void upgrade(Hero&);
};

void Mage::upgrade(Hero& ref)
{
    ref.intelligence++;
}

class Thief
{
public:
    void upgrade(Hero&);
};

void Thief::upgrade(Hero& ref)
{
    ref.dexterity++;
}

class Warrior
{
public:
    void upgrade(Hero&);
};

void Warrior::upgrade(Hero& ref)
{
    ref.endurance++;
}

class Berserker
{
public:
    void upgrade(Hero&);
};

void Berserker::upgrade(Hero& ref)
{
    ref.strength++;
}


void Hero::generateFile()
{
    string filename = name + ".txt";
    ofstream file;
    file.open(filename, ios::out | ios::trunc);
    file << name << "\n"
        << strength << "\n"
        << dexterity << "\n"
        << endurance << "\n"
        << intelligence << "\n"
        << charisma;
    file.close();
    cout  << "\nCharacter data saved to file.\n";
}

int main()
{
    showPrompt();
    int userChoice;
    do
    {
        cout << "Enter your choice: ";
        cin >> userChoice;
    } while (userChoice != 1 && userChoice != 2);

    Hero test;


    if (userChoice == 1)
    {
        string name;
        int str, dex, end, intel, charm;

        cout  << "\nPlease enter character attributes in digits:\n"
             << "\n";
        cout << "Name: ";
        cin >> name;
        cout << "STRENGTH: ";
        cin >> str;
        cout << "DEXTERITY: ";
        cin >> dex;
        cout << "ENDURANCE: ";
        cin >> end;
        cout << "INTELLIGENCE: ";
        cin >> intel;
        cout << "CHARISMA: ";
        cin >> charm;

        test = Hero(name, str, dex, end, intel, charm);

        test.generateFile();
    }

    if (userChoice == 2)
    {
        cout  << "\nEnter name of Character to load\n"
             << "\n";
        string name;
        cout << "Name: ";
        cin >> name;

        fstream file;
        file.open(name + ".txt", ios::in);
        if (!file.good())
        {
            cout << "File I/O Error: Couldn't load character file.\n";
            return 1;
        }
        else
        {
            int str, dex, end, intel, charm;
            file >> name >> str >> dex >> end >> intel >> charm;
            test = Hero(name, str, dex, end, intel, charm);

            cout << "Character loaded:\n\n"
                << test.getCharacterDetails() << "\n";
        }
    }
    newFeature(test);
 
}

void newFeature(Hero test)
{
    Mage mage;
    Warrior warrior;
    Berserker berserker;
    Thief thief;
    char prof = 0;
    while (prof != 'q')
    {
        bool firstTry = true;
        cout << "\nPlease Input the class of your character (Mage, Warrior, Berserker, Thief) (m, w, b, t) or (q) to quit: ";
        do
        {
            if (!firstTry)
            {
                cout << "Try again: ";
            }
            firstTry = false;
            cin >> prof;
        } while (prof != 'm' && prof != 'w' && prof != 'b' && prof != 't' && prof != 'q');

        if (prof == 'q')
        {
            continue;
        }

        cout << "\nLeveling up!\tNew stats:\n";
        switch (prof)
        {
        case 'm':
            mage.upgrade(test);
            break;
        case 'w':
            warrior.upgrade(test);
            break;
        case 'b':
            berserker.upgrade(test);
            break;
        case 't':
            thief.upgrade(test);
            break;
        }
        cout << test.getCharacterDetails() << "\n";
        test.generateFile();
    }
}

void showPrompt()
{
    cout << "RPG C++\n\n1. New Character\n2. Load Character\n\n";
}


