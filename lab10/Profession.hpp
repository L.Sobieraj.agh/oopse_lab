#ifndef Profession_hpp
#define Profession_hpp

#include "Hero.hpp"

class Profession
{
public:
    virtual void upgrade(Attributes&) = 0;
};

class Mage : public Profession
{
public:
    void upgrade(Attributes&);
};

class Warrior : public Profession
{
public:
    void upgrade(Attributes&);
};

class Berserker : public Profession
{
public:
    void upgrade(Attributes&);
};

class Thief : public Profession
{
public:
    void upgrade(Attributes&);
};

#endif