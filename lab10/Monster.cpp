#include "Monster.hpp"

Monster::Monster()
{
    int strength, dexterity, endurance, intelligence, charisma;
    strength = rand() % 10 + 1;
    dexterity = rand() % 10 + 1;
    endurance = rand() % 10 + 1;
    intelligence = rand() % 10 + 1;
    charisma = rand() % 10 + 1;
    updateAttributes("Monster", strength, dexterity, endurance, intelligence, charisma);
}

string Monster::getCharacterDetails()
{
    return name + " =>\tSTR: " + to_string(strength) + "\tDEX: " + to_string(dexterity) + "\tEND: " + to_string(endurance) + "\tINT: " + to_string(intelligence) + "\tCHA: " + to_string(charisma);
}

bool Monster::generateFile()
{
    string filename = name + ".txt";
    ofstream result;
    result.open(filename, ios::out | ios::trunc);
    result << name << "\n" << strength << "\n" << dexterity << "\n" << endurance << "\n" << intelligence << "\n" << charisma;
    result.close();
    cout << string(50, '*') << "\nCharacter file saved.\n";
    return true;
}

void Monster::updateAttributes(string n, int s, int d, int e, int i, int c)
{
    (*this).name = n;
    (*this).strength = s;
    (*this).dexterity = d;
    (*this).endurance = e;
    (*this).intelligence = i;
    (*this).charisma = c;
}