#include "Hero.hpp"

Hero::Hero()
{
    name = "noname";
    strength = 0;
    dexterity = 0;
    endurance = 0;
    intelligence = 0;
    charisma = 0;
}

Hero::Hero(string name, int str, int dex, int end, int intel, int chsm)
{
    (*this).name = name;
    (*this).strength = str;
    (*this).dexterity = dex;
    (*this).endurance = end;
    (*this).intelligence = intel;
    (*this).charisma = chsm;
}

string Hero::getCharacterDetails()
{
    return name + " =>\tSTR: " + to_string(strength) + "\tDEX: " + to_string(dexterity) + "\tEND: " + to_string(endurance) + "\tINT: " + to_string(intelligence) + "\tCHA: " + to_string(charisma);
}

bool Hero::generateFile()
{
    string outFileName = name + ".txt";
    ofstream outFile;
    outFile.open(outFileName, ios::out | ios::trunc);
    outFile << name << "\n"
        << strength << "\n"
        << dexterity << "\n"
        << endurance << "\n"
        << intelligence << "\n"
        << charisma;
    outFile.close();
    cout << string(50, '*') << "\nCharacter file saved.\n";
    return true;
}

void Hero::updateAttributes(string n, int s, int d, int e, int i, int c)
{
    (*this).name = n;
    (*this).strength = s;
    (*this).dexterity = d;
    (*this).endurance = e;
    (*this).intelligence = i;
    (*this).charisma = c;
}