#include "Hero.hpp"
#include "Profession.hpp"
#include "Monster.hpp"


void showPrompt()
{
    cout << "RPG C++:\n\n\t1. Make a new character\n\t2. Load Previously saved character\n\t3. Initialize monsters\n\t4. Quit\n\n";
}

int main()
{
    srand(time(NULL));
    showPrompt();

    int userChoice;
    do
    {
        cout << "Enter an option (1, 2, 3, 4): ";
        cin >> userChoice;
        if (userChoice == 4)
        {
            return 0;
        }
    } while (userChoice != 1 && userChoice != 2 && userChoice != 3);

    Hero heroRef;

    if (userChoice == 1)
    {
        string name;
        int strength, dexterity, endurance, intelligence, charisma;

        cout  << "\nEnter stats for new hero:\n"
             << "\n";
        cout << "Name: ";
        cin >> name;
        cout << "STRENGTH: ";
        cin >> strength;
        cout << "DEXTERITY: ";
        cin >> dexterity;
        cout << "ENDURANCE: ";
        cin >> endurance;
        cout << "INTELLIGENCE: ";
        cin >> intelligence;
        cout << "CHARISMA: ";
        cin >> charisma;

        heroRef = Hero(name, strength, dexterity, endurance, intelligence, charisma);

        heroRef.generateFile();
    }

    if (userChoice == 2)
    {
        cout  << "\nEnter name of existing character\n"
             << "\n";
        string name;
        cout << "Name: ";
        cin >> name;

        fstream outFile;
        outFile.open(name + ".txt", ios::in);
        if (!outFile.good())
        {
            cout << "Load failed\n";
            return 1;
        }
        else
        {
            int str, dex, end, intel, charm;
            outFile >> name >> str >> dex >> end >> intel >> charm;
            heroRef = Hero(name, str, dex, end, intel, charm);

            cout << "Character load successful:\n\n"
                << heroRef.getCharacterDetails() << "\n";
        }
    }

    if (userChoice == 3)
    {
        char selection = 0;
        do
        {
            Monster monsterData[5];
            cout << "Monsters created with the following stats:\n\n";
            for (auto& monst : monsterData)
            {
                cout << monst.getCharacterDetails() << "\n";
            }
            cout << "\nSave to file or retry?\n";
            do
            {
                cout << "Choose (s, r) or (q) to quit: ";
                cin >> selection;
                if (selection == 'q')
                {
                    return 0;
                }
            } while (selection != 's' && selection != 'r');

            if (selection == 's')
            {
                ofstream resultantFile;
                resultantFile.open("Monsters", ios::out | ios::trunc);

                for (auto& monst : monsterData)
                {
                    resultantFile << monst.getCharacterDetails() << "\n";
                }
                cout << "Saved monsters data.\n";
                return 0;
            }

        } while (selection == 'r');
    }

    Mage mageRef;
    Warrior warriorRef;
    Berserker berserkerRef;
    Thief thiefRef;

    char userInp = 0;
    while (userInp != 'q')
    {
        bool alreadyDone = true;
        cout << "\nPlease make a selection to associate with your hero, Mage (m), Warrior (w), Berserker (b), Thief (t)\n";
        do
        {
            cout << "Choose (m, w, b, t) or (q) to quit: ";
            cin >> userInp;
        } while (userInp != 'm' && userInp != 'w' && userInp != 'b' && userInp != 't' && userInp != 'q');

        if (userInp == 'q')
        {
            continue;
        }

        cout << "\nYour Character has been updated:\n";
        switch (userInp)
        {
        case 'm':
            mageRef.upgrade(heroRef);
            break;
        case 'w':
            warriorRef.upgrade(heroRef);
            break;
        case 'b':
            berserkerRef.upgrade(heroRef);
            break;
        case 't':
            thiefRef.upgrade(heroRef);
            break;
        }
        cout << heroRef.getCharacterDetails() << "\n";
        heroRef.generateFile();
    }

    return 0;
}

