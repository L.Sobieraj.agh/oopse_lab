#ifndef Attributes_hpp
#define Attributes_hpp

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>

using namespace std;

class Mage;
class Warrior;
class Berserker;
class Thief;

class Attributes
{
    friend class Mage;
    friend class Warrior;
    friend class Berserker;
    friend class Thief;

protected:
    int strength, dexterity, endurance, intelligence, charisma;
    string name;

public:
    virtual string getCharacterDetails() = 0;
    virtual bool generateFile() = 0;
    virtual void updateAttributes(string, int, int, int, int, int) = 0;
};

#endif