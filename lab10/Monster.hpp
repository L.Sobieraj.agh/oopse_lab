#ifndef Monster_hpp
#define Monster_hpp

#include "Attributes.hpp"

class Monster : public Attributes
{
public:
    Monster();
    string getCharacterDetails();
    bool generateFile();
    void updateAttributes(string, int, int, int, int, int);
};

#endif