#include "Profession.hpp"

void Mage::upgrade(Attributes& attr)
{
    attr.intelligence += 1;
}

void Warrior::upgrade(Attributes& attr)
{
    attr.endurance += 1;
}

void Berserker::upgrade(Attributes& attr)
{
    attr.strength += 1;
}

void Thief::upgrade(Attributes& attr)
{
    attr.dexterity += 1;
}