#include "Hero.hpp"
#include "Profession.hpp"
#include "Monster.hpp"
#include "Battle.hpp"

void showPrompt();
void battlePrompt();
void charGen(Hero&);
void retChar(Hero&);
bool monsterBuilder(array<Monster, 5>&);
void upgradeHero(Hero&);
bool encounter(Hero&, array<Monster, 5>&);

int main()
{
    showPrompt();
    srand(time(NULL));

    int selected;
    do
    {
        cout << "Make a selection (1, 2, 3, 4, 5): ";
        cin >> selected;
        if (selected == 5)
        {
            return 0;
        }
    } while (selected != 1 && selected != 2 && selected != 3 && selected != 4);

    Hero heroRef;
    array<Monster, 5> monsterList;

    if (selected == 1)
    {
        charGen(heroRef);
        upgradeHero(heroRef);

    }

    if (selected == 2)
    {
        retChar(heroRef);
        upgradeHero(heroRef);

    }

    if (selected == 3)
    {
        monsterBuilder(monsterList);
    }

    if (selected == 4)
    {
        encounter(heroRef, monsterList);
    }

    return 0;
}

void charGen(Hero& hero)
{
    string name;
    int str, dex, end, intel, charm;

    cout  << "\nNew Character Menu, Please fill the required details\n"
         << "\n";
    cout << "Name: ";
    cin >> name;
    cout << "STR: ";
    cin >> str;
    cout << "DEX: ";
    cin >> dex;
    cout << "END: ";
    cin >> end;
    cout << "INT: ";
    cin >> intel;
    cout << "CHA: ";
    cin >> charm;

    hero = Hero(name, str, dex, end, intel, charm, 0, 1);

    hero.generateFile();
}

void retChar(Hero& hero)
{
    cout  << "\nLoading character\n"
         << "\n";
    string name;
    cout << "Name: ";
    cin >> name;

    fstream existingFile;
    existingFile.open(name + ".txt ", ios::in);
    int str, dex, end, inte, cha, exp, lvl;
    existingFile >> name >> str >> dex >> end >> inte >> cha >> exp >> lvl;
    hero = Hero(name, str, dex, end, inte, cha, exp, lvl);

    cout << "Character loaded:\n\n"
        << hero.getCharacterDetails() << "\n";
   
}

bool monsterBuilder(array<Monster, 5>& monsters)
{
    char optionFromMenu = 0;
    do
    {
        monsters = array<Monster, 5>();
        cout << "Enemies generated as follows:\n\n";
        for (auto& m : monsters)
        {
            cout << m.getCharacterDetails() << "\n";
        }
        cout << "\nSave or retry?\n";
        do
        {
            cout << "Choose (s, r): ";
            cin >> optionFromMenu;
        } while (optionFromMenu != 's' && optionFromMenu != 'r');

        if (optionFromMenu == 's')
        {
            ofstream outputFile;
            outputFile.open("Monsters", ios::out | ios::trunc);
            for (auto& m : monsters)
            {
                outputFile << m.getCharacterDetails() << "\n";
            }
            cout << "Saved\n";
            return true;
        }

    } while (optionFromMenu == 'r');
    return false;
}

void upgradeHero(Hero& hero)
{
    char q = 0;
    Mage mageRef;
    Warrior warrRef;
    Berserker berserkerRef;
    Thief thiefRef;

    while (q != 'q')
    {
        cout << "\nWhat do you want to be? \n Mage (m), Warrior (w), Berserker (b), Thief (t)\n";
        do
        {
            cout << "Choose (m, w, b, t) or (q) to quit: ";
            cin >> q;
        } while (q != 'm' && q != 'w' && q != 'b' && q != 't' && q != 'q');

        if (q == 'q')
        {
            continue;
        }

        cout << "\nUpgrading\tNew Attributes:\n";
        switch (q)
        {
        case 'm':
            mageRef.upgrade(hero);
            break;
        case 'w':
            warrRef.upgrade(hero);
            break;
        case 'b':
            berserkerRef.upgrade(hero);
            break;
        case 't':
            thiefRef.upgrade(hero);
            break;
        }
        cout << hero.getCharacterDetails() << "\n";
        hero.generateFile();
    }
}

bool encounter(Hero& hero, array<Monster, 5>& monsters)
{
    battlePrompt();

    int batChoice;
    do
    {
        cout << "Choose (1, 2): ";
        cin >> batChoice;
    } while (batChoice != 1 && batChoice != 2);

    if (batChoice == 1)
    {
        charGen(hero);
    }

    if (batChoice == 2)
    {
        retChar(hero);
    }

    char q = 0;
    cout << "What do you want to be? Mage (m), Warrior (w), Berserker (b), Thief (t)\n";
    do
    {
        cout << "Choose (m, w, b, t): ";
        cin >> q;
    } while (q != 'm' && q != 'w' && q != 'b' && q != 't');

    Profession* profession;
    if (q == 'm')
    {
        profession = new Mage();
    }
    if (q == 'w')
    {
        profession = new Warrior();
    }

    cout  << "\n"
        << "Making monsters for fight\n"
         << "\n\n";

    monsterBuilder(monsters);

    for (auto& monster : monsters)
    {
        if (q == 'm')
        {
            Battle<Mage> battle(hero, monster);
            battle.startEncounter();
        }
        if (q == 'w')
        {
            Battle<Warrior> battle(hero, monster);
            battle.startEncounter();
        }
        if (q == 'b')
        {
            Battle<Berserker> battle(hero, monster);
            battle.startEncounter();
        }
        if (q == 't')
        {
            Battle<Thief> battle(hero, monster);
            battle.startEncounter();
        }

        char cont = 0;
        do
        {
            cout << "Press 'c' to continue: ";
            cin >> cont;
        } while (cont != 'c');
    }

    cout  << "\nEnd Fight\n";
    if (!hero.generateFile())
    {
        return false;
    }

    return true;
}

void showPrompt()
{
    cout << "RPG C++:\n\n"
        "\t1. Create new character\n"
        "\t2. Load character\n"
        "\t3. Create enemy monsters for fighting\n"
        "\t4. Start Battle\n"
        "\t5. Quit\n\n";
}

void battlePrompt()
{
    cout  << "\n"
        << "Character Selection\n"
         << "\n"
        << "Please choose an option:\n\n"
        "\t1. Create new character\n"
        "\t2. Load character\n\n";
}
