#include "Monster.hpp"

Monster::Monster()
{
    int strength, dexterity, endurance, intelligence, charisma, exper, levl;
    strength = rand() % 10 + 1;
    dexterity = rand() % 10 + 1;
    endurance = rand() % 10 + 1;
    intelligence = rand() % 10 + 1;
    charisma = rand() % 10 + 1;
    exper = rand() % 10 + 5;
    levl = 1;
    updateAttributes("Monster", strength, dexterity, endurance, intelligence, charisma, exper, levl);
}

string Monster::getCharacterDetails()
{
    return name + " =>\tSTR: " + to_string(strength) + "\tDEX: " + to_string(dexterity) + "\tEND: " + to_string(endurance) + "\tINT: " + to_string(intelligence) + "\tCHA: " + to_string(charisma) + "\tEXP: " + to_string(xp);
}

bool Monster::generateFile()
{
    string outFileName = name + ".txt";
    ofstream result;
    result.open(outFileName, ios::out | ios::trunc);
    if (!result.good())
    {
        cout << "Save failed\n";
        return false;
    }
    else
    {
        result << name << "\n"
            << strength << "\n"
            << dexterity << "\n"
            << endurance << "\n"
            << intelligence << "\n"
            << charisma;
        result.close();
        cout << string(32, '-') << "\nSaved\n";
    }
    return true;
}

void Monster::updateAttributes(string n, int s, int d, int e, int i, int c, int x, int l)
{
    (*this).name = n;
    (*this).strength = s;
    (*this).dexterity = d;
    (*this).endurance = e;
    (*this).intelligence = i;
    (*this).charisma = c;
    (*this).xp = x;
    (*this).level = l;
}