#ifndef Hero_hpp
#define Hero_hpp

#include "Attributes.hpp"

class Hero : public Attributes
{
public:
    Hero();
    Hero(string, int, int, int, int, int, int, int);
    string getCharacterDetails();
    bool generateFile();
    void updateAttributes(string, int, int, int, int, int, int, int);
};

#endif