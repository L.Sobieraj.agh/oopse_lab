#include "Hero.hpp"


string Hero::getCharacterDetails()
{
    return name + " =>\tSTR: " + to_string(strength) + "\tDEX: " + to_string(dexterity) + "\tEND: " + to_string(endurance) + "\tINT: " + to_string(intelligence) + "\tCHA: " + to_string(charisma) + "\tEXP: " + to_string(xp) + "\tLVL: " + to_string(level);
}

bool Hero::generateFile()
{
    string outFileName = name + ".txt";
    ofstream result;
    result.open(outFileName, ios::out | ios::trunc);
    if (!result.good())
    {
        cout << "save failed\n";
        return false;
    }
    else
    {
        result << name << "\n"
            << strength << "\n"
            << dexterity << "\n"
            << endurance << "\n"
            << intelligence << "\n"
            << charisma << "\n"
            << xp << "\n"
            << level;
        result.close();
        cout << string(50, '*') << "\nCharacter file saved.\n";
    }
    return true;
}


Hero::Hero()
{
    name = "noname";
    strength = 0;
    dexterity = 0;
    endurance = 0;
    intelligence = 0;
    charisma = 0;
    xp = 0;
    level = 1;
}

Hero::Hero(string name, int str, int dex, int end, int intel, int chsma, int xperience, int lv)
{
    (*this).name = name;
    (*this).strength = str;
    (*this).dexterity = dex;
    (*this).endurance = end;
    (*this).intelligence = intel;
    (*this).charisma = chsma;
    (*this).xp = xperience;
    (*this).level = lv;
}

void Hero::updateAttributes(string n, int s, int d, int e, int i, int c, int x, int l)
{
    (*this).name = n;
    (*this).strength = s;
    (*this).dexterity = d;
    (*this).endurance = e;
    (*this).intelligence = i;
    (*this).charisma = c;
    (*this).xp = x;
    (*this).level = l;
}