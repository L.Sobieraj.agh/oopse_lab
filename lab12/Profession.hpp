#ifndef Profession_hpp
#define Profession_hpp

#include "Hero.hpp"

class Profession
{
public:
    virtual void upgrade(Attributes&) = 0;
    virtual bool chooseWinner(Attributes&, Attributes&) = 0;
};

class Berserker : public Profession
{
public:
    void upgrade(Attributes&);
    bool chooseWinner(Attributes&, Attributes&);
};

class Thief : public Profession
{
public:
    void upgrade(Attributes&);
    bool chooseWinner(Attributes&, Attributes&);
};


class Mage : public Profession
{
public:
    void upgrade(Attributes&);
    bool chooseWinner(Attributes&, Attributes&);
};

class Warrior : public Profession
{
public:
    void upgrade(Attributes&);
    bool chooseWinner(Attributes&, Attributes&);
};

#endif