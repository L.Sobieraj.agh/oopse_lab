#ifndef Attributes_hpp
#define Attributes_hpp

#include <fstream>
#include <iostream>
#include <string>
#include <array>
#include <time.h>

using namespace std;

class Mage;
class Warrior;
class Berserker;
class Thief;

template <class T>
class Battle;

class Attributes
{
    friend class Mage;
    friend class Warrior;
    friend class Berserker;
    friend class Thief;
    friend class Battle<Mage>;
    friend class Battle<Warrior>;
    friend class Battle<Berserker>;
    friend class Battle<Thief>;

protected:
    int strength, dexterity, endurance, intelligence, charisma;
    int xp, level;
    string name;

public:
    virtual string getCharacterDetails() = 0;
    virtual bool generateFile() = 0;
    virtual void updateAttributes(string, int, int, int, int, int, int, int) = 0;
};

#endif