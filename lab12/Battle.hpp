#ifndef Battle_hpp
#define Battle_hpp

#include "Hero.hpp"
#include "Monster.hpp"

template <class T>
class Battle
{
    Hero& heroInBattle = 0;
    Monster& monsterInBattle = 0;
    T profClass;

public:
    Battle(Hero& h, Monster& m) : heroInBattle{ h }, monsterInBattle{ m }
    {
    }

    void startEncounter()
    {
        cout << string(32, '=') << "\nFighting Starts\n";
        cout << heroInBattle.getCharacterDetails() << "\tvs.\t"
            << monsterInBattle.getCharacterDetails() << "\n";
        if (profClass.chooseWinner(heroInBattle, monsterInBattle))
        {
            heroInBattle.xp += monsterInBattle.xp;
            cout << heroInBattle.name << " wins\n";

            while (heroInBattle.xp >= 10)
            {
                profClass.upgrade(heroInBattle);
                heroInBattle.level += 1;
                heroInBattle.xp -= 10;
                cout << heroInBattle.name << " levels up!\n"
                    << heroInBattle.getCharacterDetails() << "\n";
            }
        }
        else
        {
            heroInBattle.xp -= monsterInBattle.xp;
            if (heroInBattle.xp < 0)
            {
                heroInBattle.xp = 0;
                heroInBattle.level -= 1;
                cout << heroInBattle.name << " has downgraded\n";
                if (heroInBattle.level < 1)
                {
                    heroInBattle.level = 1;
                }
            }
            cout << "Hero Lost\n";
        }
    }
};

#endif