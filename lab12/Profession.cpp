#include "Profession.hpp"

void Mage::upgrade(Attributes& attr)
{
    attr.intelligence += 1;
}

bool Mage::chooseWinner(Attributes& attr1, Attributes& attr2)
{
    return attr1.intelligence > attr2.intelligence;
}

void Warrior::upgrade(Attributes& attr)
{
    attr.endurance += 1;
}

bool Warrior::chooseWinner(Attributes& attr1, Attributes& attr2)
{
    return attr1.endurance > attr2.endurance;
}

void Berserker::upgrade(Attributes& attr)
{
    attr.strength += 1;
}

bool Berserker::chooseWinner(Attributes& attr1, Attributes& attr2)
{
    return attr1.strength > attr2.strength;
}

void Thief::upgrade(Attributes& attr)
{
    attr.dexterity += 1;
}

bool Thief::chooseWinner(Attributes& attr1, Attributes& attr2)
{
    return attr1.dexterity > attr2.dexterity;
}